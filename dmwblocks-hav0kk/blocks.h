//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"^c#ebcb8b^^b#ebcb8b^^c#3b4252^ ", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	5,		0},

  {"^c#bf616a^^b#bf616a^^c#3b4252^ ", "sb-cpu",                                                  2,    0},

	{"^c#81A1C1^^b#81A1C1^^c#3b4252^ ", "date '+%I:%M'",					            5,		0},
	{" ", "date '+%d/%m/%y'",					                                                    5,		0},

  {"^c#2e3440^","",                                                         -1,   0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 3;
